class unsc_marine_equip
{
  name = "UNSC Standard Loadout";

  resupply[] = {
    {ACE_fieldDressing, 12}, //FIELD DRESSING
    {ACE_elasticBandage, 12}, //ELASTIC BANDAGE
    {ACE_packingBandage, 12}, //PACKING BANDAGE
    {ACE_splint, 4}, //SPLINT
    {BIT_AidKit, 2}, //AID KIT
    {OPTRE_36Rnd_95x40_Mag_Tracer, 20}, //BR55
    {OPTRE_60Rnd_762x51_Mag_Tracer, 20}, //MA5B
    {OPTRE_8Rnd_127x40_Mag, 10}, //M6B
    {TCF_4Rnd_145x114_Mag_HVAPT, 5}, //SRS99C
    {TCF_4Rnd_145x114_Mag_APFSDST, 5}, //SRS99C
    {OPTRE_200Rnd_95x40_Box_Tracer, 5}, //M73
    {OPTRE_M41_Twin_AI, 6}, //M41 SSR
    {3Rnd_SmokeRed_Grenade_shell, 5}, //GL
    {3Rnd_SmokeGreen_Grenade_shell, 5}, //GL
    {3Rnd_Smoke_Grenade_shell, 5}, //GL
    {OPTRE_3Rnd_SmokeRed_Grenade_shell, 5}, //GL
    {OPTRE_3Rnd_SmokeGreen_Grenade_shell, 5}, //GL
    {OPTRE_3Rnd_Smoke_Grenade_shell, 5}, //GL
    {1Rnd_HE_Grenade_shell, 5}, //GL
    {OPTRE_M9_Frag, 10}, //FRAG
    {OPTRE_M2_Smoke, 10}, //SMOKE
    {OPTRE_FC_BubbleShield, 1} //BUBBLE SHIELD
  };

  medical[] = {
    {DMNS_Biofoam, 5},
    {BIT_AidKit, 5},
    {ACE_salineIV, 10},
    {ACE_salineIV_500, 20},
    {ACE_bandage, 50},
    {ACE_quikclot, 50},
    {ACE_elasticBandage, 30},
    {ACE_packingBandage, 30},
    {ACE_morphine, 20},
    {ACE_epinephrine, 20},
    {ACE_tourniquet, 10},
    {ACE_splint, 10}
  };


  class rm // BR55 - Rifleman
  {
    primary[] = {
      {"OPTRE_MA5B", "optre_ma5_smartlink", "", "", ""}
    };

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_60Rnd_762x51_Mag_Tracer", 15},
      {"OPTRE_M9_Frag", 2},
      {"OPTRE_M2_Smoke", 2}
    };

    pack[] = {};
  };

  class gr : rm // MA5BGL - Grenadier
  {
    primary[] = {
          {"OPTRE_MA5BGL", "optre_ma5_smartlink", "", "", ""}
    };

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_60Rnd_762x51_Mag_Tracer", 10},
      {"OPTRE_3Rnd_SmokeRed_Grenade_shell", 2},
      {"OPTRE_3Rnd_SmokeGreen_Grenade_shell", 2},
      {"OPTRE_3Rnd_Smoke_Grenade_shell", 2},
      {"1Rnd_HE_Grenade_shell", 4},
      {"OPTRE_M9_Frag", 6},
      {"OPTRE_M2_Smoke", 2}
    };
  };

  class lat : rm // MAB5 - Rifleman (LAT)
  {
    primary[] = {
      {"OPTRE_MA5B", "optre_ma5_smartlink", "", "", ""}
    };
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_60Rnd_762x51_Mag_Tracer", 15},
      {"OPTRE_M2_Smoke", 1},
      {"OPTRE_M9_Frag", 5},
      {"OPTRE_M41_Twin_AI", 2}
    };

    secondary = "OPTRE_M41_SSR";
  };

  class ar // M73 - Autorifleman
  {
    primary[] = {{"OPTRE_M73", "optre_m73_smartlink", "", "", ""}};

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_200Rnd_95x40_Box_Tracer", 6}
    };

    pack[] = {};
  };

  class aar // MAB5 - Autorifleman Assistant
  {
    primary[] = {{"OPTRE_MA5B", "optre_ma5_smartlink", "", "", ""}};

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_60Rnd_762x51_Mag_Tracer", 15},
      {"OPTRE_200Rnd_95x40_Box_Tracer", 5},
      {"OPTRE_M2_Smoke", 2}
    };

    pack[] = {};
  };

  class cls // BR55 - Medic
  {
    medic = 1;
    primary[] = {{"OPTRE_BR55", "optre_br55hb_scope", "", "", ""}};

    items[] = {
      CLS,
      {"ACRE_PRC343", 1},
      {"OPTRE_36Rnd_95x40_Mag_Tracer", 15}
    };
  };

  class eng : rm // BR55 - Combat Engineer
  {
    primary[] = {{"OPTRE_BR55", "optre_br55hb_scope", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_36Rnd_95x40_Mag_Tracer", 10},
      {"OPTRE_M2_Smoke", 4},
      {"OPTRE_FC_BubbleShield", 3 }, 
      {"UNSCMine_Range_Mag", 2}, 
      {"DemoCharge_Remote_Mag", 2}, 
      {"ACE_Clacker", 1 }, 
      {"ACE_Fortify", 1 } 
    };
  };

  class dm // Sniper
  {
    primary[] = {
      {"OPTRE_SRS99C", "optre_srm_sight", "", "", ""}
    };

    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"TCF_4Rnd_145x114_Mag_APFSDST", 5},
      {"TCF_4Rnd_145x114_Mag_HVAPT", 5},
      {"OPTRE_M9_Frag", 2},
      {"OPTRE_M2_Smoke", 2}
    };

    pack[] = {};
  };

  class vc // Vehicle Crew
  {
    primary[] = {{"OPTRE_BR55", "optre_br55hb_scope", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 1},
      {"OPTRE_36Rnd_95x40_Mag_Tracer", 8},
      {"OPTRE_M2_Smoke", 2}
    };
    pack[] = {};
  };

  class hp // Heli Pilot
  {
    primary[] = {{"OPTRE_BR55", "optre_br55hb_scope", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 2},
      {"OPTRE_36Rnd_95x40_Mag_Tracer", 8},
      {"OPTRE_M2_Smoke", 2}
    };
    pack[] = {};
  };

  class hc // Heli Crew
  {
    primary[] = {{"OPTRE_BR55", "optre_br55hb_scope", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"OPTRE_36Rnd_95x40_Mag_Tracer", 8},
      {"OPTRE_M2_Smoke", 2}
    };
    pack[] = {};
  };

  class co : rm // Platoon Lead
  {
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 1},
      {"OPTRE_60Rnd_762x51_Mag_Tracer", 15},
      {"OPTRE_M2_Smoke", 2}
    };
  };

  class fac // FAC
  {
    primary[] = {{"OPTRE_MA5B", "optre_ma5_smartlink", "", "", ""}};
    items[] = {
      FAK,
      {"ACRE_PRC343", 1},
      {"ACRE_PRC152", 2},
      {"OPTRE_60Rnd_762x51_Mag_Tracer", 15},
      {"OPTRE_M2_Smoke", 2}
    };
  };

  class sl : co // Squad Lead
  {
  };

  class tl : co // Team Lead
  {
  };
};