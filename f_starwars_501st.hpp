class starwars_501st
{
  name = "501st Clone Trooper Legion";

  class base
  {
    uniform[] = { JLTS_CloneArmor_501 };
    helmet[] = { JLTS_CloneHelmetP2_501 };
  };

  class rm : base // Rifleman
  {
    vest[] = { JLTS_CloneVestKama };
    pack[] = { JLTS_Clone_backpack_DC };
  };
  class gr : base // Grenadier
  {
    vest[] = { JLTS_CloneVestKama };
    pack[] = { JLTS_Clone_backpack_DC };
  };
  class lat : base // Rifleman (LAT)
  {
    vest[] = { JLTS_CloneVestKama };
    pack[] = { JLTS_Clone_backpack_DC };
  };
  class ar : base // Autorifleman
  {
    vest[] = { JLTS_CloneVestKama };
    pack[] = { JLTS_Clone_backpack_DC };
  };
  class aar : base // Autorifleman Assistant
  {
    vest[] = { JLTS_CloneVestKama };
    pack[] = { JLTS_Clone_backpack_DC };
  };
  class cls // Combat Life Saver
  {
    uniform[] = { JLTS_CloneArmor_501_medic };
    helmet[] = { SWLB_P2_SpecOps_501st_Helmet };
    vest[] = { JLTS_CloneVestKama };
    pack[] = { JLTS_Clone_backpack_medic };
  };

  class eng : base // Combat Engineer
  {
    vest[] = { JLTS_CloneVestKama };
    pack[] = { JLTS_Clone_backpack_eod };
  };
  class dm : base // Designated Marksman
  {
    vest[] = { TCF_Marks_ODST_Red };
    pack[] = { JLTS_Clone_backpack_DC };
  };

  class hp // Heli Pilot
  {
    helmet[] = { JLTS_CloneHelmetAB_501 };
    uniform[] = { JLTS_CloneArmor_501_airborne };
    vest[] = { JLTS_CloneVestAirborne_501 };
    pack[] = { JLTS_Clone_backpack_DC };
  };

  class hc : hp // Heli Crew
  {
  };

  class vc : hp // Vehicle Crew
  {
    binocular = JLTS_CloneBinocular;
  };

  class co // Platoon Leader
  {
    uniform[] = { JLTS_CloneArmorMC_501_Vaughn };
    helmet[] = { JLTS_CloneHelmetP2_501 };
    vest[] = { JLTS_CloneVestCommander_501 };
    pack[] = { JLTS_Clone_backpack_DC };
    binocular = JLTS_CloneBinocular;
  };

  class tl : co // Team Leader
  {
  };

  class fac : co // Forward Air Controller
  {
  };

  class sl : co // Squad Leader
  {
  };
};
