// MA5B                         rm
// MA5B-GL                      gr
// MA5B + LAT                   lat
// SRS99C                       dm
// M73                          ar


class unsc_snow
{
  name = "UNSC Marines (Snow Camo)";

  class base
  {
    uniform[] = { OPTRE_UNSC_Army_Uniform_SNO };
    helmet[] = { OPTRE_UNSC_CH252_Helmet_SNO };
  };

  class rm : base // Rifleman
  {
    vest[] = { OPTRE_UNSC_M52A_Armor_Rifleman_SNO };
    pack[] = { OPTRE_UNSC_Rucksack };
  };
  class gr : base // Grenadier
  {
    vest[] = { OPTRE_UNSC_M52A_Armor_Rifleman_SNO };
    pack[] = { OPTRE_UNSC_Rucksack };
  };
  class lat : base // Rifleman (LAT)
  {
    vest[] = { OPTRE_UNSC_M52A_Armor_Rifleman_SNO };
    pack[] = { OPTRE_UNSC_Rucksack };
  };
  class ar : base // Autorifleman
  {
    vest[] = { OPTRE_UNSC_M52A_Armor_MG_SNO };
    pack[] = { OPTRE_UNSC_Rucksack_Heavy };
  };
  class aar : base // Autorifleman Assistant
  {
    vest[] = { OPTRE_UNSC_M52A_Armor_MG_SNO };
    pack[] = { OPTRE_UNSC_Rucksack_Heavy };
  };
  class cls : base // Combat Life Saver
  {
    helmet[] = { OPTRE_UNSC_CH252_Helmet_SNO_MED };
    vest[] = { OPTRE_UNSC_M52A_Armor_Medic_SNO };
    pack[] = { OPTRE_UNSC_Rucksack_Medic };
  };

  class eng : base // Combat Engineer
  {
    vest[] = { OPTRE_UNSC_M52A_Armor_Rifleman_SNO };
    pack[] = { OPTRE_UNSC_Rucksack_Heavy };
  };
  class dm : base // Designated Marksman
  {
    vest[] = { OPTRE_UNSC_M52A_Armor_Marksman_SNO };
    pack[] = { OPTRE_UNSC_Rucksack };
  };

  class hp : base // Heli Pilot
  {
    vest[] = { OPTRE_UNSC_M52A_Armor3_SNO };
    pack[] = { OPTRE_UNSC_Rucksack };
  };

  class hc : hp // Heli Crew
  {
    vest[] = { OPTRE_UNSC_M52A_Armor3_SNO };
    pack[] = { OPTRE_UNSC_Rucksack };
  };

  class co : rm // Platoon Leader
  {
    binocular = OPTRE_Smartfinder;
  };

  class tl : co // Team Leader
  {
  };

  class fac : co // Forward Air Controller
  {
    vest[] = { OPTRE_UNSC_M52A_Armor_Rifleman_SNO };
    binocular = OPTRE_Smartfinder;
    pack[] = { OPTRE_UNSC_Rucksack };
  };

  class sl : co // Squad Leader
  {
  };

  class vc : base // Vehicle Crew
  {
    helmet[] = { OPTRE_UNSC_CH252_Helmet_MAR };
    binocular = OPTRE_Smartfinder;
    vest[] = { OPTRE_UNSC_M52A_Armor3_SNO };
    pack[] = { OPTRE_UNSC_Rucksack };
  };
};
