class unsc_odst
{
  name = "UNSC Oribital Strike Drop Troopers";

  class base
  {
    uniform[] = { 19thu_U_ODST };
    helmet[] = { 19th_H_ODST };
  };

  class rm : base // Rifleman
  {
    vest[] = { 19th_marine_vest_rifleman };
    pack[] = { OPTRE_ILCS_Rucksack_Black };
  };
  class gr : base // Grenadier
  {
    vest[] = { 19th_marine_vest_rifleman };
    pack[] = { OPTRE_ILCS_Rucksack_Black };
  };
  class lat : base // Rifleman (LAT)
  {
    vest[] = { 19th_marine_vest_rifleman };
    pack[] = { OPTRE_ILCS_Rucksack_Black };
  };
  class ar : base // Autorifleman
  {
    vest[] = { TCF_Demo_ODST_Red };
    pack[] = { OPTRE_ILCS_Rucksack_Heavy };
  };
  class aar : base // Autorifleman Assistant
  {
    vest[] = { TCF_Demo_ODST_Red };
    pack[] = { OPTRE_ILCS_Rucksack_Heavy };
  };
  class cls : base // Combat Life Saver
  {
    helmet[] = { 19th_H_ODST };
    vest[] = { TCF_Marks_ODST_Red };
    pack[] = { OPTRE_ILCS_Rucksack_Medical };
  };

  class eng : base // Combat Engineer
  {
    vest[] = { TCF_Demo_ODST_Red };
    pack[] = { OPTRE_ILCS_Rucksack_Heavy };
  };
  class dm : base // Designated Marksman
  {
    vest[] = { TCF_Marks_ODST_Red };
    pack[] = { OPTRE_ILCS_Rucksack_Black };
  };

  class hp : base // Heli Pilot
  {
    vest[] = { 19th_marine_vest_rifleman };
    pack[] = { OPTRE_ILCS_Rucksack_Black };
  };

  class hc : hp // Heli Crew
  {
    vest[] = { 19th_marine_vest_rifleman };
    pack[] = { OPTRE_ILCS_Rucksack_Black };
  };

  class co : rm // Platoon Leader
  {
    binocular = OPTRE_Smartfinder;
  };

  class tl : co // Team Leader
  {
  };

  class fac : co // Forward Air Controller
  {
    vest[] = { 19th_marine_vest_rifleman };
    binocular = OPTRE_Smartfinder;
    pack[] = { OPTRE_ILCS_Rucksack_Black };
  };

  class sl : co // Squad Leader
  {
  };

  class vc : base // Vehicle Crew
  {
    helmet[] = { 19th_H_ODST };
    binocular = OPTRE_Smartfinder;
    vest[] = { 19th_marine_vest_rifleman };
    pack[] = { OPTRE_ILCS_Rucksack_Black };
  };
};
